#!/bin/sh

if [ -d build ]; then
  echo "Directory build/ exists. Removing it...";
  rm -r build/;
fi

grunt && ruby tasks/updatemanifest.rb && cd build && jekyll build;

if [ "$1" = "-s" ]
then
  jekyll serve;
fi

# exit status 0 indicates a success
echo "Script exit code: $?"
