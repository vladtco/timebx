/**
* @fileoverview
* Implements funtionality of interracting with Web Audio API
* 
**/

function AudioAPI() {
	this.context = null;
	this.memoryBuffer = null;
	this.soundSource = null;
	this.apiSupport = false;
	try {
		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		context = new AudioContext();
		this.apiSupport = true;
	} catch(error) {
		if(error instanceof TypeError)
			console.error("Web Audio API is not supported in this browser");
	}
};

// every time gets called modifies the buffer
AudioAPI.prototype.loadSoundFile = function(url) {
	var request = new XMLHttpRequest();
	// asynchronous download
	request.open('GET', url, true);
	// audio file data is binary
	request.responseType = 'arraybuffer';

	// reference to context
	var loader = this;

	request.onload = function() {
		// Decode aynchronously not blocking main thread
		context.decodeAudioData(request.response, 
			function(buffer) {
				if(!buffer) {
					throw new Error('error decoding file data: ' + url);
				}
				loader.memoryBuffer = buffer;
			}, 
			function() {
        		console.error("Error downloading a file.");
      		}
      	);
	}
	request.send();
};

AudioAPI.prototype.playSound = function(time) {
	if(time === false) {
		if(!soundSource.stop)
			soundSource.stop = soundSource.noteOff;
		soundSource.stop(0);
		return;
	}
	soundSource = context.createBufferSource(); // create sound source
	soundSource.buffer = this.memoryBuffer; // tell the source which sound to play
	// Turn on looping
	soundSource.loop = true;
	// connect the source with context destination (speakers)
	soundSource.connect(context.destination);
	// fallback for older browsers
	if (!soundSource.start)
      soundSource.start = soundSource.noteOn; 
	soundSource.start(time); // time is a delay before playing
};

AudioAPI.prototype.isSupported = function() {
	return this.apiSupport;
}