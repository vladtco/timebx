/**
 * @fileoverview
 * Provides functions for interracting with localStorage
 * and synchronization with the server.
 * 
 *
 * @requires jQuery
 * @requires Modernizr  
 */

$(document).ready(function() {
	if (!Modernizr)
		throw new Error("Storage.js requires Modernizr.");

	if(!view) {
		throw new Error("global.js: This file requires view.js");
	}

	appCacheStorage.init();
});

var appCacheStorage = {

	appCache: window.applicationCache,

	// abstraction for hash storage
	storage: function() {

		// create reference to the object for closures after return statement
		var self = this;

		_objects = [];

		return {
			// write operation
			pushHash: function(hashObj) {
				// adding cloned object to an array
				var newHashObj = {};
				for (var key in hashObj) {
					newHashObj[key] = hashObj[key];
				}
				self._objects.push(newHashObj);
			},

			updateHash: function(hashObjId, hashObj) {
				// overwriting of properties
				for (var key in hashObj) {
					self._objects[hashObjId - 1][key] = hashObj[key];
				}
			},

			// save array of items
			loadHashes: function(hashObjArr) {
				self._objects = self._objects.concat(hashObjArr);
			},

			// read and return one item
			getHash: function(hashObjId) {
				return self._objects[hashObjId];
			},

			// read and return all items
			getHashes: function() {
				return self._objects;
			},

			deleteHash: function(hashObjId) {
				self._objects.splice(hashObjId - 1, 1); // ids count starts from 1
				for (var i = 0; i < self._objects.length; i++) {
					self._objects[i].box_id = i + 1;
				}
			},

			getSize: function() {
				return self._objects.length;
			}
		};
	}(), // instant evaluation into the object

	// Storage initialization
	init: function() {
		var self = this;

		// check for requirements
		if (!Modernizr.applicationcache) {
			console.error("storage.js: This browser does not support the application cache.");
			return;
		}

		if (!Modernizr.localstorage) {
			console.error("storage.js: This browser does not support local storage.");
			return;
		}

		if (!window.JSON) {
			console.error("storage.js: This browser does not support JSON.");
			return;
		}

		// attach an event to the change of network connection
		$(window)
			.on('online', function() {
				self.updateForNetStatus(true);
			})
			.on('offline', function() {
				self.updateForNetStatus(false);
			});

		// check if a new cache is available on the page load
		window.addEventListener('load', function(e) {
			self.appCache.addEventListener('updateready', function() {
					// force new version
					if (self.appCache.status == self.appCache.UPDATEREADY) {
						if( confirm('A new version of this site is available. Load it?') ) {
							window.location.reload();
						 	console.log("storage.js: A new version of this site was loaded.");
						}
					}
			}, false);
		}, false);

		self.appCache.addEventListener('error', function() {
			console.error('storage.js: Cache failed to update!');
		}, false);

		// load data from local storage to render on the screen
		self.loadState();
	},

	saveTimebox: function(timeboxObj) {
		if(timeboxObj.box_id > this.storage.getSize()) {
			this.storage.pushHash(timeboxObj); // new object was added
		} else {
			this.storage.updateHash(timeboxObj.box_id, timeboxObj);
		}
		
		// modify local data
		this.saveState();

		// push changes to server
		if(navigator.onLine)
			this.syncWithServer();
	},

	getTimebox: function(boxId) {
		return this.storage.getHash(boxId - 1);
	},

	getSize: function() {
		return this.storage.getSize();
	},

	deleteTimebox: function(boxId) {
		this.storage.deleteHash(boxId);
		if (this.isEmpty()) {
			localStorage.clear();
		} else {
			this.saveState();
		}

		if(navigator.onLine)
			this.syncWithServer();	
	}, 

	isEmpty: function() {
		if (0 !== this.storage.getHashes().length)
			return false;
		return true;
	},

	saveState: function() {
		localStorage.setItem("timeboxList", JSON.stringify(this.storage.getHashes()));
	},

	loadState: function() {
		if(localStorage.getItem("timeboxList")) {
			this.storage.loadHashes(JSON.parse(localStorage.getItem("timeboxList")));
			view.postTimebox("tbody.tibx-timebox-grid", this.storage.getHashes());
		}

		// TODO figure out how to show the actual list
		// who wins server or app? or nobody?
	},

	// every time when connection is available sync with the server
	updateForNetStatus: function(connected) {
		if (connected) {
			this.syncWithServer();
			view.showAlert("App is online.");
		} else {
			view.showAlert("App is offline.");
		}
	},

	syncWithServer: function() {
		var self = this;

		/* $.ajax({
			
			// variables
			url: "", // address of API  //How do I tell server that I want to add something???
			async: true, // default value
			cache: false, // force requested pages not to be cached by the browser
			timeout: 500, // milliseconds
			context: self, // defines value of this for all callback functions
			processData: true, // default: data sent with the request should be transfered into query string
			type: "POST",
			dataType: "json", // data expected back from the server | returns JS object | data parsed in a strict manner
			crossDomain: false,
			contentType: "application/json; charset=utf-8",
			header: { Connection: "keep-alive" }, // an object of additional header key/value pairs to send along with request using XMLHttpRequest transport
			data: JSON.stringify(self.storage.getHashes()), // data send to the server | format "{"hello":"world"}" | processed by $.param | processData: false to prevent
			// serializes string to accommondate modern scripting languages and frameworks
			statusCode: {
				404: function() {
					console.log("resource wasn't found");
				},
				500: function() {
					console.log("the server is down");
				}
			},
			
			// functuons

			beforeSend: function(jqXHR) {
				console.log("beforeSend() was called.");
			},

			complete: function(jqXHR, textStatus) { // XMLHTTPRequest
				console.log("Request complete");
			},

			success: function(data, textStatus, jqXHR) {
				if (!data || !("length" in data)) {
					console.log("Storage.js: Unable to sync with server");
					return;
				}
				// TODO on success show message
			},

			error: function(jqXHR, textStatus, errorThrown) {
				console.log("Storage.js: Unable to sync with server: " + errorThrown);
			}
		
		}).done(function(data) { // data is a data received back
			console.log("Data synchronized!");
		});*/

	}

};