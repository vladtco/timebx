/**
 * @fileoverview
 * Backbone of an application. Processes form for adding new timeboxes. Checks 
 * input for correctness, sanitizes it and sends data to server.F
 *
 *
 *
 * @requires jQuery.js
 * @requires html_sanitize.js
 * @requires storage.js
 * @requires view.js
 */

( function($) {

	if (typeof view === "undefined")
		throw new Error("application.js: This file requires view.js");

	if (typeof appCacheStorage === "undefined")
		throw new Error("application.js: This file requires storage.js");

	if (typeof $().slider() === "undefined")
		throw new Error("application.js: This file requires jQueryUI loaded.");

	if (typeof html === "undefined" || typeof html4 === "undefined")
		throw new Error("application.js: This file requires html-sanitizer.js");

	var timer = undefined;
	var soundsEnabled = false;

	// create object that will contain all form fields
	// sigleton
	var timeboxObj = function() {
		
		// create reference to the object for closures after return statement
		var self = this; 

		// private data
		_parameters = { 
			box_id: undefined, 
			timestamp: undefined,
			deliverable: undefined, 
			hours: undefined, 
			minutes: undefined,
			break_length: undefined,
			break_frequency: undefined,
			pomodoro_technique: false
		};

		return {
			
			setParameter: function(key, value) {
				self._parameters[key] = value;
			},

			getParameter: function(key) {
				return self._parameters[key];
			},

			getParameters: function() {
				return self._parameters;
			},
		
			destruct: function() {
				self._parameters.box_id = undefined;
				self._parameters.timestamp 
				self._parameters.deliverable = undefined;
				self._parameters.hours = undefined;
				self._parameters.minutes = undefined;
				self._parameters.break_length = undefined;
				self._parameters.break_frequency = undefined;
				self._pomodoro_technique = false;
			}
		};
	}(); // instant evaluation into the object

	// ask for confirmation on exit or reload when timer is running
	// not completely supported in Firefox and Opera
	window.onbeforeunload = function() {
		if( $('div.current-timebox').css('display') === 'block' )
			return "You a timebox in progress. if you decide to proceed the timer will be reset.";
	};

	// validate timebox deliverable on click
	$('#tibx-save-edit-timebox').on('click', function(e){

		// set parameter and sanitize input
		timeboxObj.setParameter("deliverable", 
			html_sanitize($('textarea#tibx-timebox-deliverable').val(),
				// urlTransformer
				function(url) {
					if(/^https?:\/\//.test(url)) 
						return url;
				},
				// nameIdClassTransformer
				function(id) {
					return; // disables id's, names and classes
				}
		));

		if (timeboxObj.getParameter("deliverable").length === 0) {
			addAlertToModal("tibx-alert-empty-deliverable", "You didn\'t write deliverable.");
			return;
		} else {
			$('div.tibx-alert-empty-deliverable').remove();
		}
		
		timeboxObj.setParameter("hours", $( ".slider-hours" ).slider("value"));
		timeboxObj.setParameter("minutes", $( ".slider-minutes" ).slider("value"));

		if ( timeboxObj.getParameter("hours") === 0 && timeboxObj.getParameter("minutes") === 0 ) {
			addAlertToModal("tibx-alert-no-time-set", "You didn\'t set time for a new timebox.");
			return;
		} else {
			$('div.tibx-alert-no-time-set').remove();
		}

		timeboxObj.setParameter("break_length", $( '.slider-break-length' ).slider('value'));
		timeboxObj.setParameter('break_frequency', $( '.slider-break-frequency' ).slider('value'));
		timeboxObj.setParameter('pomodoro_technique', $('.pomodoro-technique').is(':checked'));

		// validation complete 
		$('div#addTimeboxModal').modal('hide');

		// set the time of object creation or modification in UTC since January 1, 1970
		timeboxObj.setParameter("timestamp", new Date().getTime());

		// store data
		appCacheStorage.saveTimebox(timeboxObj.getParameters());

		// add timebox to timebox grid
		view.postTimebox("tbody.tibx-timebox-grid", timeboxObj.getParameters());

		// check if there is a timebox in progress
		// if yes disable start button
		if( $('div.current-timebox').css('display') === 'block' ) {
			var startButtonArray = document.getElementsByClassName("tibx-start-timebox");
			var lastAddedStartButton = startButtonArray[startButtonArray.length - 1];
			lastAddedStartButton.setAttribute("disabled", "disabled");
		}
	});

	$('.tibx-sounds').on('click', function(e) {
		if (soundsEnabled)
			$(this).children('span.glyphicon-volume-up').removeClass('glyphicon-volume-up').addClass("glyphicon-volume-off"); 
		else
			$(this).children('span.glyphicon-volume-off').removeClass('glyphicon-volume-off').addClass("glyphicon-volume-up");
		soundsEnabled = !soundsEnabled;
		if (timer)
			timer.switchSounds();
	});	

	$('.pomodoro-technique').on('click', function(e) {
		$(".slider-break-length").slider("option", "value", 0);
		$(".slider-break-frequency").slider("option", "value", 0);
	});

	$('#addTimeboxModal').on('show.bs.modal', function(e) { // event is fired up before users sees anything
		// if edit button fired up an event that means we are in the edit mode
		var timeboxId = $(e.relatedTarget).parent().attr('data-tibx-timebox-index');
		if (typeof timeboxId === 'string' ) {
			// insert required values and attributes
			var timeboxData = appCacheStorage.getTimebox(timeboxId);
			for (var prop in timeboxData) {
				if(timeboxData.hasOwnProperty(prop)) {
					timeboxObj.setParameter(prop, timeboxData[prop]);
				}
			}
			$('h4.modal-title').text('Edit Timebox');
			// changed text to val. because text() wasn't changing value if modal was shown more then once
			$('textarea#tibx-timebox-deliverable').val(timeboxObj.getParameter("deliverable"));
			view.initializeSliders(
				[timeboxObj.getParameter("hours"),
				timeboxObj.getParameter("minutes"),
				timeboxObj.getParameter("break_length"),
				timeboxObj.getParameter("break_frequency")]
			);
			if (timeboxObj.getParameter('pomodoro_technique')) {
				$('.pomodoro-technique').prop('checked', 'true');	
			}	
			$('#tibx-save-edit-timebox').text('Save Changes');
		} else {
			$('h4.modal-title').text('Add Timebox');
			view.initializeSliders([0, 0, 0, 0]);
			$('#tibx-save-edit-timebox').text('Save');
			timeboxObj.setParameter("box_id", appCacheStorage.getSize() + 1);
		} 
	});

	$('#addTimeboxModal').on('hidden.bs.modal', function(e) { // changes happen when modal is hidden from user
		// empty out fields, unset dropdowns and delete error messages
		// remove error messages if they exist
		$('div.tibx-alert-empty-deliverable').remove();
		$('div.tibx-alert-no-time-set').remove();

		$('textarea#tibx-timebox-deliverable').val('');
		$(".pomodoro-technique").prop('checked', false);

		timeboxObj.destruct();
	});

	$('button.tibx-pause-timebox').on('click', function(e) {
		if (e.currentTarget.innerHTML === 'stop') {
			timer.stopTimer();
      document.title = 'Timebx: paused';
			view.convertButton('tibx-pause-timebox', 'resume', 'btn-danger', 'btn-success');
		} else {
			timer.startTimer();
			view.convertButton('tibx-pause-timebox', 'stop', 'btn-success', 'btn-danger');	
		}
			
	});
	
	$('button.tibx-finish-timebox').on('click', function(e) {		
		timer.resetTimer();
		timer = undefined;
		
		if($('.tibx-pause-timebox').text() === 'resume')
			view.convertButton('tibx-pause-timebox', 'stop', 'btn-success', 'btn-danger'); 

		if ($('.tibx-pause-timebox').attr('disabled'))
			$('.tibx-pause-timebox').removeAttr('disabled');
		
		if (soundsEnabled) {
			soundsEnabled = false;
			$('button.tibx-sounds').children('span.glyphicon-volume-up').removeClass('glyphicon-volume-up').addClass("glyphicon-volume-off");
		}

		// clear out fields
		$('div.tibx-current-timebox-deliverable').text('');		
		$('span.tibx-timer-section').each(function() {
			$(this).text('--');
		});

		
		// hide panel
		$('div.current-timebox').css('display', 'none');
		
		// remove highlight
		timeboxContainer = document.getElementsByClassName("danger")[0];
		$(timeboxContainer).removeClass("danger");

		// enable start buttons everywhere
		$('button.tibx-start-timebox').each(function() {
			$(this).removeAttr("disabled", "disabled");
		});

		// enable all controllers for the timebox
		buttonGroup = document.getElementsByClassName("tibx-active-timebox")[0];
		$(buttonGroup).children().removeAttr('disabled');
		$(buttonGroup).removeClass('tibx-active-timebox');
	
    document.title = 'TimeBx'; 
  });

	$('tbody.tibx-timebox-grid').on('click', 'button.tibx-start-timebox', function(e){
		// get timeboxId and store it in local variable
		var timeboxId = this.parentNode
			.getAttribute('data-tibx-timebox-index');

		var timeboxObj = appCacheStorage.getTimebox(timeboxId);
		// caption for timer
		$('h3.tibx-current-timebox-deliverable').text(timeboxObj.deliverable);

		timer = new Timer(timeboxObj, soundsEnabled);
		timer.startTimer();

		// show current timebox panel with all data
		$('div.current-timebox').css('display', 'block');

		// hide start button on all timeboxes
		$('button.tibx-start-timebox').each(function() {
			$(this).attr("disabled", "disabled");
		});

		// disable all buttons for this timebox
		this.parentNode.childNodes[3].setAttribute("disabled", "disabled");
		this.parentNode.childNodes[5].setAttribute("disabled", "disabled");

		// highlight the field of running timebox
		$('tr.tibx-timeboxid-' + timeboxId.toString()).addClass('danger');
		
		// put a marker on tr
		$(this.parentNode).addClass('tibx-active-timebox');

	});


	// Delete Button
	$('tbody.tibx-timebox-grid').on('click', 'button.tibx-delete-timebox', function(e){ // find parent element that doesn't have a duplicate
		
		// store timebox id in local variable
		var timeboxId = this.parentNode
			.getAttribute('data-tibx-timebox-index');
		
		// delete that table line from the DOM
		$('tr.' + 'tibx-timeboxid-' + timeboxId).remove();

		// delete that record from the local storage
		appCacheStorage.deleteTimebox(timeboxId);

		 
		// if no timebox left show panel with message
		if (appCacheStorage.isEmpty()) {
			$('tr.tibx-no-timeboxes-msg').css('display', 'table-row');
		} else {
			// update indexes on the screen
			view.updateIndicesAttributes("tibx-timebox", "btn-group-options");
		}
		
	});

	// add alert to modal if it is not already there
	function addAlertToModal(alertClass, errorText) {
		if ($('div.' + alertClass).length === 0)
			$('<div class="alert alert-danger modal-alert ' + alertClass + '"><button type="button" class="close"' +
				'data-dismiss="alert">&times;</button>Oh snap! ' + errorText + '</div>')
				.insertBefore('form.add-timebox');
	}

})(jQuery);
