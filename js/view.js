/**
 * @fileoverview
 * Responsible for all interractive UI of the
 * application main screen.
 *
 * @requires jQuery
 */

var view = {
	
	// list a new timebox or update already listed
	postTimebox: function(container, timeboxes) {
		// hide no timebox message
		if ($('tr.tibx-no-timeboxes-msg').css('display') === 'table-row')
			$('tr.tibx-no-timeboxes-msg').css('display', 'none');
		if ( !(timeboxes instanceof Array) ) {
			// if id less than the number of timeboxes listed
			// it means that the timebox was edited and text nodes have to be replaced
			if(timeboxes.box_id <= this.countPostedTimeboxes("tibx-timebox")) {
				// get table row container
				timeboxContainer = document.getElementsByClassName("tibx-timeboxid-" + timeboxes.box_id.toString())[0]; // id is unique there will be only one container		
				timeboxContainer.childNodes[3].innerHTML = "<span>" + timeboxes.deliverable + "</span>";
				if (timeboxes.hours > 0)
					timeboxContainer.childNodes[5].innerHTML = "<span>" + timeboxes.hours.toString() + "h&nbsp" + timeboxes.minutes.toString() + "min" + "</span>";
				else
					timeboxContainer.childNodes[5].innerHTML = "<span>" + timeboxes.minutes.toString() + "min" + "</span>";
				timeboxContainer.childNodes[7].innerHTML = "<span>" + timeboxes.break_length.toString() + "min" + "</span>";
				timeboxContainer.childNodes[9].innerHTML = "<span>" + timeboxes.break_frequency.toString() + "</span>";
				return;
			}
			timeboxes = new Array(timeboxes);
		}
		// get reference to the template inside the DOM
		template = $.trim( $('script#timebox-template').html() ); // delete all whitespaces in the beginning and end
		// otherwise new timeboxes have to posted
		timeboxes.forEach(function(item){ // item, index, array

			var temp = template.replace( /{{index}}/ig, item.box_id.toString() )
						.replace( /{{description}}/ig, item.deliverable )
						.replace( /{{minutes}}/ig, item.minutes.toString() + "min" )
						.replace( /{{breaks}}/ig, item.break_length.toString() + "min" )
						.replace( /{{frequency}}/ig, item.break_frequency.toString() );

			if (item.hours > 0)
				temp = temp.replace( /{{hours}}/ig, item.hours.toString() + "h&nbsp;");
			else
				temp = temp.replace( /{{hours}}/ig, "");

			// after replacements are done new object temp has to be added to html document
			$(container).append(temp);
		});
	},

	updateIndicesAttributes: function(containerClass1, containerClass2) {
		timeboxesArray = document.getElementsByClassName(containerClass1); // change class of table row and index 
		var newClassName = "";
		for (var i = 0; i < timeboxesArray.length; i++) {
			newClassName = containerClass1 + " tibx-timeboxid-" + (i+1).toString();
			if( timeboxesArray[i].className.length > newClassName.length ) {
				timeboxesArray[i].className = newClassName + timeboxesArray[i].className.substr(newClassName.length);
			} else {
				timeboxesArray[i].className = newClassName;
			}
			timeboxesArray[i].childNodes[1].innerHTML = (i+1).toString() + "."; // label with index is a second child | first child is a node content
		}
		
		buttonGroupsArray = document.getElementsByClassName(containerClass2); // change attribute
		for (i = 0; i < buttonGroupsArray.length; i++) {
			buttonGroupsArray[i].setAttribute("data-tibx-timebox-index", (i+1).toString());
			
		}
	
	},

	countPostedTimeboxes: function (containerClass) {
		return document.getElementsByClassName(containerClass).length;
	},

	showAlert: function(message) {
		$('<div class="text-center alert alert-warning alert-dismissable tibx-user-alert"><span>' + message + 
			'</span><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>')
			.insertBefore('header');
		setTimeout(
			function() {
				document.getElementsByClassName("tibx-user-alert")[0].remove();
			}, 
			5000); // wait for 5 seconds  
	},

	convertButton: function(buttonClass, buttonText, oldClass, newClass) {
		$('button.' + buttonClass).removeClass(oldClass).addClass(newClass).html(buttonText);
	},

	// render sliders in modal
	initializeSliders: function(values) {
		$( ".slider-hours" ).slider({ // initialization
			min: 0,
			max: 5,
			value: values[0],
			range: "min",
			// this method is necessary for the first run 
			create: function() {
				$("#display-hours").html($(this).slider("value") + " h");
			},
			change: function() {
				$("#display-hours").html($(this).slider("value") + " h");
			}
		});

		$( ".slider-minutes" ).slider({
			min: 0,
			max: 60,
			value: values[1],
			range: "min",
			create: function() {
				$("#display-minutes").html($(this).slider("value") + " min");
			},
			change: function() {
				$("#display-minutes").html($(this).slider("value") + " min");	
			}
		});

		$( ".slider-break-length" ).slider({
			min: 0,
			max: 30,
			value: values[2],
			range: "min",
			create: function() {
				if ( values[2] > 0) {
					$("#display-break").html(values[2].toString() + " min");
				} else {
					$("#display-break").html('length: 0 min');
				}
			},
			change: function() {
				if ($(this).slider("value") > 0) {
					$("#display-break").html($(this).slider('value') + ' min');
					if ($('.slider-break-frequency').slider('value') === 0) {
						$('.slider-break-frequency').slider({ value: 1 });
					}
				} else {
					$("#display-break").html('length: 0 min');
				}
			}
		});

		$( ".slider-break-frequency" ).slider({
			min: 0,
			max: 12,
			value: values[3],
			range: "min",
			create: function() {
				if (values[3] > 0) {
					$("#display-frequency").html(values[3]);	
				} else {
					$("#display-frequency").html('frequency: 0');
				}
			},
			change: function() {
				if ($(this).slider("value") > 0) {
					$("#display-frequency").html($(this).slider("value"));
				} else {
					$("#display-frequency").html('frequency: 0');	
				}
			}
		});
	}

};