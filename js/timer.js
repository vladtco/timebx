/**
* @fileoverview
* Implements timer functionality inside an app.
*
* Using Singleton Pattern & Combination of Prototype and Construction Pattern
*
* @requires JQuery
* @requires view
* @requires audio
*
**/

var audioAPI = undefined;

window.onload = init;

function init() {
	if (typeof AudioAPI !== 'undefined') {
		audioAPI = new AudioAPI();
		if (audioAPI.isSupported()) {
                  // Browser implementors are encouraged to return "maybe"
                  // unless the type can be confidently established as being
                  // supported or not.
                  if (Modernizr.audio.mp3 === 'probably') {
                    console.log("Loading mp3");
                    audioAPI.loadSoundFile("sounds/alarm.mp3")
                  } else {
                    console.log("Loading webm");
                    audioAPI.loadSoundFile("sounds/alarm.webm");
                  }  
		} else {
			$('button.tibx-sounds').css('display', 'none');
		}
	}
}

function Timer(timeBoxObj, enableSounds) {
	// save parameters | not using prototype since these are object properties
	// all these variables are set by the user
	this.countdownTime = {
		hours: timeBoxObj.hours,
		minutes: timeBoxObj.minutes  
	};

	this.displayedTime = {
		hours: undefined,
		minutes: undefined,
		seconds: undefined
	}

	if (timeBoxObj.pomodoro_technique) {
		this.breakLength = 5;
		this.breakFrequency = 1; // to pass entry condition
		this.breakUnit = 25; // break every 25 min
	} else {
		this.breakLength = timeBoxObj.break_length;
		this.breakFrequency = timeBoxObj.break_frequency;
		var totalMinutes = this.countdownTime.hours*60 + this.countdownTime.minutes
		if (this.breakFrequency === 1 || this.breakLength > totalMinutes) {	
			this.breakUnit = Math.floor(totalMinutes / 2); // break in the middle of time frame
		} else if (this.breakFrequency > 1 && totalMinutes > this.breakLength) {
			this.breakUnit = Math.floor(totalMinutes / this.breakFrequency);
		}
	}

	this.remainingTime = this.countdownTime.hours*60*60*1000 + this.countdownTime.minutes*60*1000;
	this.enableSounds = enableSounds;
	this.isPlaying = false;
	this.intervalID = undefined;
	this.timeoutID = undefined;
	this.pomodoroTechnique = {
		on: timeBoxObj.pomodoro_technique,
		cycleCount: 0
	}
	this.isTicking = false;

	this.renderTime = function(timeObj) {
		// rendering only when value gets changed
		var titleTime = '';
    for(prop in this.displayedTime) {
			if ( this.displayedTime[prop] != timeObj[prop] ) {
				this.displayedTime[prop] = timeObj[prop];
				if ( 10 > this.displayedTime[prop] ) {
					document.getElementById(prop).innerHTML = 
					"0" + this.displayedTime[prop].toString();
				} else {
					document.getElementById(prop).innerHTML = 
					this.displayedTime[prop].toString();
				}
			}
      if( 10 > timeObj[prop] ) {
        titleTime = titleTime + '0' + timeObj[prop];
      } else {
        titleTime += timeObj[prop];
      }
      if ( 'seconds' != prop ) {
        titleTime += ':';
      }
		}
    document.title = titleTime;
	},

	this.renderTime({
		hours: timeBoxObj.hours,
		minutes: timeBoxObj.minutes,
		seconds: 0
	});

};

Timer.prototype = {
	
	constructor: Timer,

	startTimer: function() {
		this.flipState();
		this.startTime = this.elapsedTime();
		// reference for Timer object
		var self = this;
		this.intervalID = setInterval(function() {
			self.countDown();
			self.startTime = self.elapsedTime();
		}, 1000);
	},

	stopTimer: function() {
		this.flipState();
		clearInterval(this.intervalID);
	},

	resetTimer: function() {
		if (this.getState()) {
			this.stopTimer();
		} else {
			if (this.remainingTime > 0 && typeof this.timeoutID === 'number') {
				// clearTimeout so timer won't restart by itself when the break time is up
				clearTimeout(this.timeoutID);
				this.timeoutID = undefined;
				$("div.tibx-timer-alert").remove();
			}
		}
	}, 

	breakSignaller: function(remainingTimeConverted) { 
		var breakLength = this.breakLength;
		if(this.breakFrequency > 0 && this.breakLength > 0 && this.breakUnit > 0) {
		var setTimeInMinutes = this.countdownTime.hours * 60 + this.countdownTime.minutes;
		var remainingTimeInMinutes = remainingTimeConverted.hours * 60 + remainingTimeConverted.minutes + 1; // round up for 60 sec
		if( (setTimeInMinutes != remainingTimeInMinutes) && (remainingTimeInMinutes != 0) &&
			(((setTimeInMinutes - remainingTimeInMinutes)) % this.breakUnit) === 0 ) {
				
				if(this.pomodoroTechnique.on) {
					this.pomodoroTechnique.cycleCount += 1
					if(this.pomodoroTechnique.cycleCount === 4) {
						breakLength = 15;
						this.pomodoroTechnique.cycleCount = 0;
					}
				}

				$("button.tibx-pause-timebox").attr("disabled", "disabled");

				this.soundNotification(); // turn sound on
        document.title = 'Timebx: break';
				alert("Break time!" + "\n\n" + "Switch your attention to something else for " + 
					breakLength.toString() + " min.");
				this.soundNotification(); // and turn it off 
				this.stopTimer();

				$("<div class=\"alert alert-success tibx-timer-alert\">" +
					"<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
  					"<strong>Enjoy your break. |^_^|</strong></div>").insertBefore('.current-timebox');

				var self = this;
				this.timeoutID = setTimeout(function() {
					$('div.tibx-timer-alert').remove();
					self.soundNotification();
					alert("Break is over!" + "\n\n" + "Now you have to come back to what you were doing.");
					self.soundNotification();
					self.startTimer();
					$('button.tibx-pause-timebox').removeAttr("disabled");
					self.timeoutID = undefined;					 
				}, breakLength * 60 * 1000);
		}
	}
	},

	countDown: function() {
		this.remainingTime = Math.round(this.remainingTime - (this.elapsedTime() - this.startTime));
			
		remainingTimeConverted = this.convertTime(this.remainingTime);
		// call only when minutes change not seconds
		if(this.minNow != remainingTimeConverted.minutes) {
			this.breakSignaller(remainingTimeConverted);
			this.minNow = remainingTimeConverted.minutes;
		}

		if( 0 >= this.remainingTime ) {
			this.soundNotification();
			alert("Time is up!");
			this.soundNotification();
			this.stopTimer();
			$('button.tibx-start-timer').css('display', 'none');
			remainingTimeConverted = {
				hours: 0,
				minutes: 0,
				seconds: 0
			};
		}

		this.renderTime({
			hours: remainingTimeConverted.hours,
			minutes: remainingTimeConverted.minutes,
			seconds: remainingTimeConverted.seconds
		});
	},

	flipState: function() {
		this.isTicking = !this.isTicking;
	},

	getState: function() {
		return this.isTicking;
	},

	// uses HR TIME API if it is supported by client
	elapsedTime: function() {
		return window.performance.now() ||
         window.performance.mozNow()    ||
         window.performance.msNow()     ||
         window.performance.oNow()      ||
         window.performance.webkitNow() ||
         function() { return new Date().getTime(); };
	},

	// convert time in human readable format
	convertTime: function(timeInMs) {
		h = Math.floor(timeInMs / 3600000);
		timeInMs = timeInMs - h*60*60*1000;
		min = Math.floor(timeInMs / 60000);
		timeInMs = timeInMs - min*60*1000;
		// display correction 
		sec = Math.round(timeInMs / 1000);
		return {
			hours: h,
			minutes: min,
			seconds: sec
		};
	},

	switchSounds: function() {
		this.enableSounds = !this.enableSounds;
	},

	soundNotification: function() {
		try {
			if(this.isPlaying) {
				audioAPI.playSound(false);
				this.isPlaying = !this.isPlaying;
			} else if (!this.isPlaying && this.enableSounds) {
				audioAPI.playSound(0);
				this.isPlaying = !this.isPlaying;
			}
		} catch(error) {
			console.error('timer.js: Error playing sound.');
			$('button.tibx-sounds').css('display', 'none');
			this.enableSounds = false;
			this.isPlaying = false;
		}
	}
};
