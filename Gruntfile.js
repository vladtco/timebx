/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;' +
      ' Licensed <%= pkg.license %> */\n',
    // Task configuration.
    copy: {
      options: {
        mode: true
      },
      // task target
      main: {
        files: [
          {expand: true, nonull: true, src: ['offline.appcache', '_config.yml', 'css/bootstrap.min.css', 'css/images/*.png',
           'fonts/Ubuntu-R.ttf', 'fonts/glyphicons-halflings-regular.ttf', 'fonts/glyphicons-halflings-regular.woff', 
           'Gemfile', 'robots.txt', 'sounds/alarm.webm', 'sounds/*.mp3'], dest: 'build/'},
          {expand: true, nonull: true, src: ['js/vendor/*.min.js'], dest: 'build/'}
        ] 
      },
      modifiable: {
        src: ['index.html'],
        dest: 'build/'
      }
    },
    htmlmin: {
     options: {
       removeComments: true,
       collapseWhitespace: true,
       collapseBooleanAttributes: true,
       removeAttributeQuotes: true,
       removeEmptyAttributes: true
     },
     build: {
       files: {
         'build/index.html': ['build/index.html'],
         'build/404.html': ['404.html'], 
       }
     }
    }, 
   useminPrepare: {
     html: 'index.html',
     options: {
       dest: '.', 
       flow: {
         // Workflow configuration
         steps: {}, 
         post: []
       }
     }  
   },
   usemin: {
     html: 'build/index.html',
     options: {
       assetsDirs: ['build/css', 'build/js']
     }
   },
   cssmin: {
      options: {
        report: 'gzip'
      },
      my_css: {
        files: {
          'build/css/global.min.css': ['css/global.css']
        }, 
        options: {
          banner: '<%= banner %>',
        }
      },
      vendor_css: {
        files: {
         'build/css/jquery-ui-1.10.3.slider.min.css': ['css/jquery-ui-1.10.3.slider.css']
        }
      }
    }, 
    concat: {
      options: {
        stripBanners: {
          block: true,
          line: true
        },
        separator: ';',
        process: false
      },
      build: {
        src: ['js/view.js', 'js/storage.js', 'js/sanitizer.js', 'js/audio.js', 'js/timer.js', 'js/application.js'],
        dest: '.tmp/concat/js/build.js',
        nonull: true
      }
    },
    uglify: {
      options: {
        // prevent changes to functions and variable names
        mangle: false,
        report: 'gzip',
        banner: '<%= banner %>',
        preserveComments: false
      },
      my_scripts: {
        src: '<%= concat.build.dest %>',
        dest: 'build/js/main.min.js',
        options: {  
          // basic source map output 
          sourceMap: 'build/js/source-map.js', 
          sourceMappingURL: 'source-map.js', 
        }
      },
      vendor_scripts: {
       files: {
         'build/js/vendor/jquery-ui-1.10.3.slider.min.js': ['js/vendor/jquery-ui-1.10.3.slider.js'],
         'build/js/vendor/ga.min.js': ['js/vendor/ga.js']
       }, 
       options: {
         banner: ''
       }
      },
    },
    watch: {
      all: {
        files: ['index.html', '404.html', 'css/global.css', 'js/*.js'],
        tasks: ['copy:modifiable', 'useminPrepare', 'usemin', 'htmlmin:build', 'cssmin:my_css', 'concat:build', 'uglify:my_scripts'],
        options: {
          // also can be 'added', 'deleted'
          event: ['changed'],
          // Default port is 35729
          livereload: true
        } 
      },
      html: {
        files: ['index.html', '404.html'],
        tasks: ['copy:modifiable', 'useminPrepare', 'usemin', 'htmlmin:build'],
        options: {
          event: ['changed'],
          livereload: true
        } 
      }, 
      css: {
        files: ['css/global.css'],
        tasks: ['cssmin:my_css'],
        options: {
          event: ['changed'],
          livereload: true
        } 
      }, 
      scripts: {
        files: ['js/*.js'], 
        tasks: ['concat:build', 'uglify:my_scripts'],
        options: {
          event: ['changed'],
          livereload: true
        } 
      }
    },
    connect: {
      server: {
        options: {
          base: 'build/',
          keepalive: true
        }
      }
    }
});

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');  
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Run grunt connect:keepalive on the port 8000
  grunt.loadNpmTasks('grunt-contrib-connect');

  // Default task.
  grunt.registerTask('default', ['copy', 'useminPrepare', 'usemin', 'htmlmin', 'cssmin', 'cssmin', 'concat', 'uglify']);

};
