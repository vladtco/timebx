Into
====

My first single-page web application.

What is TimeBx? 

TimeBx is an app for personal time management. It utilizes timeboxing project 
management technique.

Work expands so as to fill the time available for its completion. TimeBx helps 
you cope with this fact by coupling these two constrains: time and task scope. 

Working on smaller things for shorter periods of time will boosts your 
creativity and focus. And on top of that, crossing out more items off you to-do 
list keeps you motivated to make more done.

Timeboxing is a popular project management technique. Each box has it's time and 
deliverables. It allows keep time constant and reduce scope of a task when deadline
cannot be met. 

http://en.wikipedia.org/wiki/Timeboxing


Pomodoro Technique
==================

This technique breaks down work time into 25 min in length, separated by short 
breaks (3-5 min). Every forth interval take a longer break (15-25 min).

In Pomodoro Technique mode number of break intervals is equal rounded quotient 
from division where divident is alloted time for a timebox and denominator equal 
25. Short break time is 5 min. Long break (every 4th 25 min interval) is 15 min.
Therefore expected person's efficiency for 100 min of work time is 70%.  


Application Cache
=================

Application design manifests "offline first".

Application cache enables offline mode. Check out 
offline.appcache file.

if you look at the header of offline.appcache file you'll see :v[number]. Version
number is included here for a reason so that if any file among files for caching 
changes it will get updated in the local browser cache which is not possible
without updating of the manifest.

To clear cache in Chrome go to chrome://appcache-internals/ and select "Clear 
browser data...".

*Application Cache statuses:*

* UNCACHED # application cache object is not fully initialized
* IDLE # application cache is not currently in the process of being updated
* CHECKING # the manifest is being fetched and checked for updates
* DOWNLOADING # resources are being downloaded to be added to the cache
* UPDATEREADY # there is a new version of application cache available. 
* OBSOLETE # application cache is now obsolete

*Once an application is offline it remains cached until one of the following 
happens:*

	1. The user clears their browser's data storage for your site.
	2. The manifest file is modified.
	3. The app cache is programatically updated.

	appCache.update(); # check and download cache (takes a page reload)
	
	appCache.swapCache(); # update cache (takes a page reload to show updated content)

When applications are cached, simply updating the resources (files) that are used
in a web page is not enough to update the files that have been cached. You must 
update the cache manifest file itself before the browser retrieves and uses 
the updated files. You can do this programmatically using 
window.applicationCache.swapCache(), though resources that have 
already been loaded will not be affected. To make sure that resources are loaded
from a new version of the application cache, refreshing the page is ideal.

It's a good idea to set expires headers on your web server for .appcache files 
to expire immediately. This avoids the 
risk of caching manifest files. For example, in Apache you can specify such a 
configuration as follows:
ExpiresByType text/cache-manifest "access plus 0 seconds".


Application Hosting and Publishing
==================================

Using GitHub pages and Jekyll.


Responsive layout
=================

The app interface has to be responsive. I don't expect it to be used on mobile 
or tablet devices.

First, I want to identify breakpoins by width:

+ 320px(small screen devices in portrait mode)
+ 480px(for small screen devices, held in landscape mode)
+ 600px(small tablets in portrait mode)
+ 768px(IPad in portrait mode)
+ 1024px(for widescreen display, primarily laptop and desktop)
+ 1200px(for widescreen displays, primarily laptop and desktop)


Project Dependencies
====================

Tools, frameworks and libraries used in this project:

+ Bootstrap 3.0
+ JQuery 2.0.3
+ JQuery UI 1.10.3 Slider
+ Modernizr v2.6.2
+ Google Caja (for user input sanitation)
+ Jekyll

Grunt
=====

A setup adds two files to my project: package.json and the Gruntfile. 

package.json: This file is used by npm to store metadata for projects published as
npm modules.

Gruntfile: This file is named Grunt.js and is used to configure or define tasks and load
Grunt plugins. 

Showing Time in the Title
=========================

Here are two things I noticed while using the app: it doesn't pace me up and I
constantly switch to application tab to see the time. Unproductive and
distractive. Showing time in the site title aims to solve these problems. 
